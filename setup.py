#!/usr/bin/env python
# -*- encoding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Install and (nose-)test caosdb."""
from setuptools import setup, find_packages

setup(name='PyCaosDB',
      version='0.1.0',
      description='Python Interface for CaosDB',
      author='Timm Fitschen',
      author_email='timm.fitschen@ds.mpg.de',
      packages=find_packages('src'),
      package_dir={'': 'src'},
      install_requires=['lxml>=3.6.4',
                        'PyYaml>=3.12', 'future', 'PySocks>=1.6.7'],
      extras_require={'keyring': ['keyring>=13.0.0']},
      setup_requires=["pytest-runner>=2.0,<3dev"],
      tests_require=["pytest", "pytest-cov", "coverage>=4.4.2"],
      )
