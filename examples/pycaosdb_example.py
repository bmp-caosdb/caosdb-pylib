#!/usr/bin/env python3
"""A small example to get started with caosdb-pylib.

Make sure that a `pycaosdb.ini` is readable at one of the expected locations.
"""

import random

import caosdb


def main():
    """Shows a few examples how to use the CaosDB library."""
    conf = dict(caosdb.configuration.get_config().items("Connection"))
    print("##### Config:\n{}\n".format(conf))

    if conf["cacert"] == "/path/to/caosdb.ca.pem":
        print("Very likely, the path the the TLS certificate is not correct, "
              "please fix it.")

    # Query the server, the result is a Container
    result = caosdb.Query("FIND Record").execute()
    print("##### First query result:\n{}\n".format(result[0]))

    # Retrieve a random Record
    rec_id = random.choice([rec.id for rec in result])
    rec = caosdb.Record(id=rec_id).retrieve()
    print("##### Randomly retrieved Record:\n{}\n".format(rec))


if __name__ == "__main__":
    main()
